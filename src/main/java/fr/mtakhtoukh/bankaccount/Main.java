package fr.mtakhtoukh.bankaccount;

import fr.mtakhtoukh.bankaccount.bank.Account;
import fr.mtakhtoukh.bankaccount.bank.AccountRepository;
import fr.mtakhtoukh.bankaccount.util.SystemMomentProvider;
import fr.mtakhtoukh.bankaccount.util.SystemOutConsoleChannel;

import java.math.BigDecimal;

public class Main {
    public static void main(String[] args) {
        Account account = new Account(new AccountRepository(), new SystemOutConsoleChannel(), new SystemMomentProvider());

        account.deposit(BigDecimal.valueOf(170.04));
        account.deposit(BigDecimal.valueOf(50.80));
        account.withdraw(BigDecimal.valueOf(10.7));
        account.deposit(BigDecimal.valueOf(1500.56));
        account.withdraw(BigDecimal.valueOf(90.87));
        account.deposit(BigDecimal.valueOf(1.48));

        account.print();
    }
}
