package fr.mtakhtoukh.bankaccount.bank;

import fr.mtakhtoukh.bankaccount.util.ConsoleChannel;
import fr.mtakhtoukh.bankaccount.util.MomentProvider;

import java.math.BigDecimal;

public class Account implements Depositable, Withdrawable {
    private AccountRepository repository;
    private ConsoleChannel consoleChannel;
    private MomentProvider momentProvider;

    public Account(AccountRepository repository, ConsoleChannel consoleChannel, MomentProvider momentProvider) {
        this.repository = repository;
        this.consoleChannel = consoleChannel;
        this.momentProvider = momentProvider;
    }

    public void deposit(BigDecimal amount) {
        this.repository.addDeposit(amount, momentProvider.getMoment());
    }

    public void withdraw(BigDecimal amount) {
        this.repository.addWithdraw(amount, momentProvider.getMoment());
    }

    BigDecimal getBalance() {
        return this.repository.getBalance();
    }

    public void print() {
        BankStatementPrinter.print(repository.getStatements(), consoleChannel);
    }
}
