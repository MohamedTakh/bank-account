package fr.mtakhtoukh.bankaccount.bank;

import fr.mtakhtoukh.bankaccount.operations.Operation;
import fr.mtakhtoukh.bankaccount.operations.OperationType;
import fr.mtakhtoukh.bankaccount.operations.Statement;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class AccountRepository {
    static final String OVERDRAFT_EXCEPTION = "Your account can't be in overdraft!";
    static final String NEGATIVE_AMOUNT = "You can't deposit or withdraw a negative amount!";

    private List<Statement> statements = new LinkedList<>();
    BigDecimal balance = BigDecimal.ZERO;

    List<Statement> getStatements() {
        return Collections.unmodifiableList(statements);
    }

    BigDecimal getBalance() {
        return this.balance;
    }

    void addDeposit(BigDecimal amount, Instant moment) {
        if (amount.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException(NEGATIVE_AMOUNT);
        }

        balance = balance.add(amount);

        this.statements.add(
                new Statement(
                        new Operation(amount, moment, OperationType.DEPOSIT),
                        balance
                )
        );
    }

    void addWithdraw(BigDecimal amount, Instant moment) {
        if (amount.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException(NEGATIVE_AMOUNT);
        }

        if (amount.compareTo(getBalance()) > 0) {
            throw new IllegalArgumentException(OVERDRAFT_EXCEPTION);
        }

        balance = balance.subtract(amount);

        this.statements.add(
                new Statement(
                        new Operation(amount, moment, OperationType.WITHDRAW),
                        balance
                )
        );
    }
}
