package fr.mtakhtoukh.bankaccount.bank;

import fr.mtakhtoukh.bankaccount.operations.Statement;
import fr.mtakhtoukh.bankaccount.util.ConsoleChannel;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;

class BankStatementPrinter {
    static final String HEADER = String.format("%10s | %8s | %8s | %8s", "Date", "Type", "Amount", "Balance")
            + "\n" + "-----------|----------|----------|---------";

    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private BankStatementPrinter() {

    }

    static void print(List<Statement> statements, ConsoleChannel consoleChannel) {
        consoleChannel.print(HEADER);
        for (Statement statement : statements) {
            consoleChannel.print(
                    String.format("%10s | %8s | %8.2f | %8.2f",
                            LocalDateTime.ofInstant(statement.getOperationMoment(), ZoneOffset.UTC).format(TIME_FORMATTER),
                            statement.getOperationType(),
                            statement.getOperationAmount(),
                            statement.getBalance()
                    )
            );
        }
    }
}
