package fr.mtakhtoukh.bankaccount.bank;

import java.math.BigDecimal;

public interface Depositable {
    /**
     * Increase the funds on account of {@code amount}
     *
     * @param amount to add on account
     */
    void deposit(BigDecimal amount);
}
