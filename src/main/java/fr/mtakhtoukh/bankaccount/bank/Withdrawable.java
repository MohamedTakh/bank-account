package fr.mtakhtoukh.bankaccount.bank;

import java.math.BigDecimal;

public interface Withdrawable {
    /**
     * Decrease the funds on account of {@code amount}
     *
     * @param amount to add on account
     */
    void withdraw(BigDecimal amount);
}
