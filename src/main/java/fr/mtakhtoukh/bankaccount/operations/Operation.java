package fr.mtakhtoukh.bankaccount.operations;

import java.math.BigDecimal;
import java.time.Instant;

public class Operation {
    private BigDecimal amount;
    private Instant moment;
    private OperationType type;

    public Operation(BigDecimal amount, Instant moment, OperationType type) {
        this.amount = amount;
        this.moment = moment;
        this.type = type;
    }

    BigDecimal getAmount() {
        return amount;
    }

    Instant getMoment() {
        return moment;
    }

    OperationType getType() {
        return type;
    }
}
