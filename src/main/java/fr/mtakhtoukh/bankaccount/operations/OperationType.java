package fr.mtakhtoukh.bankaccount.operations;

public enum OperationType {
    DEPOSIT("Deposit"),
    WITHDRAW("Withdraw");

    private String name;

    OperationType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
