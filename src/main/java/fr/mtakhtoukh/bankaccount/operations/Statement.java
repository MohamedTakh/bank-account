package fr.mtakhtoukh.bankaccount.operations;

import java.math.BigDecimal;
import java.time.Instant;

public class Statement {
    private Operation operation;
    private BigDecimal balance;

    public Statement(Operation operation, BigDecimal balance) {
        this.operation = operation;
        this.balance = balance;
    }

    public BigDecimal getOperationAmount() {
        return operation.getAmount();
    }

    public Instant getOperationMoment() {
        return operation.getMoment();
    }

    public OperationType getOperationType() {
        return operation.getType();
    }

    public BigDecimal getBalance() {
        return balance;
    }
}
