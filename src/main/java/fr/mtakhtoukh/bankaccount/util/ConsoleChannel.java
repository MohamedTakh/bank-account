package fr.mtakhtoukh.bankaccount.util;

public interface ConsoleChannel {
    void print(String string);
}
