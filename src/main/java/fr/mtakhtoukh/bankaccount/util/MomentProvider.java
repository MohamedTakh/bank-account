package fr.mtakhtoukh.bankaccount.util;

import java.time.Instant;

public interface MomentProvider {
    Instant getMoment();
}
