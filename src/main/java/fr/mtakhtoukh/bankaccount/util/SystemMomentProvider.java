package fr.mtakhtoukh.bankaccount.util;

import java.time.Instant;

public class SystemMomentProvider implements MomentProvider {
    @Override
    public Instant getMoment() {
        return Instant.now();
    }
}
