package fr.mtakhtoukh.bankaccount.util;

public class SystemOutConsoleChannel implements ConsoleChannel {
    @Override
    public void print(String string) {
        System.out.println(string);
    }
}
