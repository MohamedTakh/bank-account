package fr.mtakhtoukh.bankaccount.bank;

import fr.mtakhtoukh.bankaccount.util.FixedMomentProvider;
import fr.mtakhtoukh.bankaccount.util.MomentProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigDecimal;

import static fr.mtakhtoukh.bankaccount.bank.AccountRepository.NEGATIVE_AMOUNT;
import static fr.mtakhtoukh.bankaccount.bank.AccountRepository.OVERDRAFT_EXCEPTION;
import static org.junit.jupiter.api.Assertions.*;

class AccountRepositoryTest {
    private AccountRepository accountRepository;
    private BigDecimal initialBalance = BigDecimal.valueOf(1000L);
    private MomentProvider momentProvider;

    @BeforeEach
    void setup() {
        accountRepository = new AccountRepositoryDouble(initialBalance);
        momentProvider = new FixedMomentProvider();
    }

    @DisplayName("A new account repository has no statements and a null balance")
    @Test
    void new_account_repository_should_be_empty_with_null_balance() {
        assertTrue(accountRepository.getStatements().isEmpty());
        assertEquals(initialBalance, accountRepository.getBalance());
    }

    @DisplayName("Deposit funds on an account should register a statement")
    @ParameterizedTest
    @ValueSource(ints = {5, 20, 30})
    void deposit_funds_on_account_repository_should_register_a_statement(int value) {
        accountRepository.addDeposit(BigDecimal.valueOf(value), this.momentProvider.getMoment());
        assertEquals(1, accountRepository.getStatements().size());
        assertEquals(initialBalance.add(BigDecimal.valueOf(value)), accountRepository.getBalance());
    }

    @DisplayName("Withdraw funds on an account should register a statement")
    @ParameterizedTest
    @ValueSource(ints = {5, 20, 30})
    void withdraw_funds_on_account_repository_should_register_a_statement(int value) {
        accountRepository.addWithdraw(BigDecimal.valueOf(value), this.momentProvider.getMoment());
        assertEquals(1, accountRepository.getStatements().size());
        assertEquals(initialBalance.subtract(BigDecimal.valueOf(value)), accountRepository.getBalance());
    }

    @DisplayName("Withdraw enough funds to have an overdraft account should raise exception")
    @Test
    void withdraw_to_many_funds_should_raise_exception() {
        assertThrows(
                IllegalArgumentException.class,
                () -> accountRepository.addWithdraw(initialBalance.add(BigDecimal.ONE), this.momentProvider.getMoment()),
                OVERDRAFT_EXCEPTION
        );
    }

    @DisplayName("Deposit a negative amount should raise exception")
    @ParameterizedTest
    @ValueSource(ints = {-1, -5, -100})
    void deposit_negative_amout_should_raise_exception() {
        assertThrows(
                IllegalArgumentException.class,
                () -> accountRepository.addDeposit(BigDecimal.TEN.negate(), this.momentProvider.getMoment()),
                NEGATIVE_AMOUNT
        );
    }

    @DisplayName("Withdraw a negative amount should raise exception")
    @ParameterizedTest
    @ValueSource(ints = {-1, -5, -100})
    void withdraw_negative_amout_should_raise_exception() {
        assertThrows(
                IllegalArgumentException.class,
                () -> accountRepository.addWithdraw(BigDecimal.TEN.negate(), this.momentProvider.getMoment()),
                NEGATIVE_AMOUNT
        );
    }
}