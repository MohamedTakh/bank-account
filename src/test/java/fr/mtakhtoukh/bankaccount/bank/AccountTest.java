package fr.mtakhtoukh.bankaccount.bank;

import fr.mtakhtoukh.bankaccount.util.FixedMomentProvider;
import fr.mtakhtoukh.bankaccount.util.InMemoryConsoleChannel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigDecimal;

import static fr.mtakhtoukh.bankaccount.bank.BankStatementPrinter.HEADER;
import static fr.mtakhtoukh.bankaccount.util.FixedMomentProvider.FIXED_DATE;
import static org.junit.jupiter.api.Assertions.assertEquals;

class AccountTest {
    private Account account;
    private BigDecimal initialBalance = BigDecimal.valueOf(1000L);
    private InMemoryConsoleChannel consoleChannel;

    @BeforeEach
    void setup() {
        consoleChannel = new InMemoryConsoleChannel();
        account = new Account(
                new AccountRepositoryDouble(initialBalance),
                consoleChannel,
                new FixedMomentProvider()
        );
    }

    @DisplayName("A new account should be empty")
    @Test
    void new_account_should_be_empty() {
        assertEquals(initialBalance, account.getBalance());
    }

    @DisplayName("The account balance should be increased by the amount of the deposit")
    @ParameterizedTest
    @ValueSource(ints = {1, 5, 10, 100})
    void balance_should_be_increased_by_deposited_amount(int value) {
        account.deposit(BigDecimal.valueOf(value));
        assertEquals(initialBalance.add(BigDecimal.valueOf(value)), account.getBalance());
    }

    @DisplayName("The account balance should be reduced by the withdrawal amount")
    @ParameterizedTest
    @ValueSource(ints = {1, 5, 10, 100})
    void balance_should_be_decreased_by_withdrawn_amount(int value) {
        account.withdraw(BigDecimal.valueOf(value));
        assertEquals(initialBalance.subtract(BigDecimal.valueOf(value)), account.getBalance());
    }

    @DisplayName("Performed deposits or withdrawals should be displayed on print")
    @ParameterizedTest
    @ValueSource(ints = {1, 5, 10, 100})
    void print_empty_statements_should_display_only_header(int value) {
        account.deposit(BigDecimal.valueOf(value));
        account.withdraw(BigDecimal.valueOf(value));

        account.print();

        assertEquals(3, consoleChannel.getStrings().size());
        assertEquals(HEADER, consoleChannel.getStrings().get(0));
        assertEquals(String.format("%s | %8s | %8.2f | %8.2f",
                FIXED_DATE.substring(0, 10),
                "Deposit",
                BigDecimal.valueOf(value),
                initialBalance.add(BigDecimal.valueOf(value))),
                consoleChannel.getStrings().get(1)
        );
        assertEquals(String.format("%s | %8s | %8.2f | %8.2f",
                FIXED_DATE.substring(0, 10),
                "Withdraw",
                BigDecimal.valueOf(value),
                initialBalance),
                consoleChannel.getStrings().get(2));
    }
}
