package fr.mtakhtoukh.bankaccount.bank;

import fr.mtakhtoukh.bankaccount.operations.Operation;
import fr.mtakhtoukh.bankaccount.operations.OperationType;
import fr.mtakhtoukh.bankaccount.operations.Statement;
import fr.mtakhtoukh.bankaccount.util.FixedMomentProvider;
import fr.mtakhtoukh.bankaccount.util.InMemoryConsoleChannel;
import fr.mtakhtoukh.bankaccount.util.MomentProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import static fr.mtakhtoukh.bankaccount.bank.BankStatementPrinter.HEADER;
import static fr.mtakhtoukh.bankaccount.util.FixedMomentProvider.FIXED_DATE;
import static org.junit.jupiter.api.Assertions.assertEquals;

class BankStatementPrinterTest {
    private List<Statement> statements;
    private InMemoryConsoleChannel printer;
    private MomentProvider momentProvider;

    @BeforeEach
    void setup() {
        this.statements = new LinkedList<>();
        this.printer = new InMemoryConsoleChannel();
        this.momentProvider = new FixedMomentProvider();
    }

    @DisplayName("Empty statement list should display only headers")
    @Test
    void print_empty_statements_should_display_only_header() {
        BankStatementPrinter.print(statements, printer);
        assertEquals(1, printer.getStrings().size());
        assertEquals(HEADER, printer.getStrings().get(0));
    }

    @DisplayName("ConsoleChannel should display all statements")
    @ParameterizedTest
    @ValueSource(ints = {5, 10, 50})
    void printer_should_display_all_statements(int bound) {
        // Given
        for (int i = 0; i < bound; i++) {
            this.statements.add(
                    new Statement(
                            new Operation(BigDecimal.valueOf(i), this.momentProvider.getMoment(), OperationType.DEPOSIT),
                            BigDecimal.ONE
                    )
            );
        }

        // When
        BankStatementPrinter.print(statements, printer);

        // Then
        assertEquals(bound + 1, printer.getStrings().size());
        for (int i = 0; i < bound; i++) {
            assertEquals(
                    String.format("%s | %8s | %8.2f | %8.2f",
                            FIXED_DATE.substring(0, 10),
                            "Deposit",
                            BigDecimal.valueOf(i),
                            1.0),
                    printer.getStrings().get(i + 1)
            );
        }
    }
}