package fr.mtakhtoukh.bankaccount.util;

import java.time.Instant;

public class FixedMomentProvider implements MomentProvider {
    private Instant instant;
    public static final String FIXED_DATE = "2018-08-17T10:15:30.00Z";

    public FixedMomentProvider() {
        this.instant = Instant.parse(FIXED_DATE);
    }

    @Override
    public Instant getMoment() {
        return this.instant;
    }
}
