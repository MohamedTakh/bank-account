package fr.mtakhtoukh.bankaccount.util;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class InMemoryConsoleChannel implements ConsoleChannel {
    private List<String> strings = new LinkedList<>();

    @Override
    public void print(String string) {
        strings.add(string);
    }

    public List<String> getStrings() {
        return Collections.unmodifiableList(strings);
    }
}
